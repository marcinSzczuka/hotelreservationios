//
//  addPersonViewController.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 20.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import UIKit

class AddPersonViewController: UIViewController {
    
    @IBOutlet weak var personText: UITextView!
    var person: Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        personText.font = UIFont(name:"Avenir", size:14)
        personText.text = person.description
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        
    }
    
}