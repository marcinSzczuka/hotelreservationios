//
//  AddReservationViewController.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 21.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import UIKit
class AddReservationViewController: UIViewController {
    
    @IBOutlet weak var reservationText: UITextView!
    var reservation: Reservation!
    let requestHelper: RequestHelper = RequestHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reservationText.font = UIFont(name:"Avenir", size:14)
        reservationText.text = reservation.description
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let addReservationViewController = segue.destinationViewController as! AddReservationViewController
        
        // show reservation list
        if (segue.identifier == "showUpdate") {
            requestHelper.updateService(reservation.services[0].id)
            reservation.services = requestHelper.getServiceListByReservationId(reservation.id)
            addReservationViewController.reservation = reservation
        }
        
        if (segue.identifier == "showDelete") {
            requestHelper.deleteService(reservation.services[0].id)
            reservation.services = requestHelper.getServiceListByReservationId(reservation.id)
            addReservationViewController.reservation = reservation

        }
    }
}