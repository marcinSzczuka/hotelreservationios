//
//  ViewController.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 19.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var objectParser: JsonParseObjectHelper = JsonParseObjectHelper()
    let requestHelper: RequestHelper = RequestHelper()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // show reservation list
        if (segue.identifier == "showReservationListSegue") {
            let reservationListViewController = segue.destinationViewController as! ReservationListViewController
            
            let json : NSString
            var jsonData : NSData? = nil
            if let filepath = NSBundle.mainBundle().pathForResource("reservation", ofType: "json") {
                do {
                    json = try NSString(contentsOfFile: filepath, usedEncoding: nil) as String
                    jsonData = NSData(contentsOfFile: filepath)
                    print(json)
                } catch {
                    // contents could not be loaded
                }
            } else {
                // example.txt not found!
            }
            
            do {
                
                // create post request
                let url = NSURL(string: "http://localhost:8080/api/select")!
                let request = NSMutableURLRequest(URL: url)
                request.HTTPMethod = "POST"
                
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.HTTPBody = jsonData
                
                
                let task = NSURLSession.sharedSession().dataTaskWithRequest(request){ data, response, error in
                    print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                    // convert NSData to 'AnyObject'
                    reservationListViewController.jsonData = data
                    reservationListViewController.viewDidLoad()
                }
                task.resume()
                
            }
        }
        
        // addPerson
        if (segue.identifier == "showAddPersonSegue") {
            
            var person: Person = Person()
            
            
            let addPersonVievController = segue.destinationViewController as! AddPersonViewController
            
                let personId = requestHelper.addPerson()
                person = requestHelper.getPerson(personId)
                person.address = requestHelper.getAddress(person.addressId)
                addPersonVievController.person = person
        }
        
        // addReservation
        if (segue.identifier == "showAddReservationSegue") {
            var reservation: Reservation = Reservation()
            var room: Room = Room()
            var person: Person = Person()
            var services: [Service] = []
            var address: Address = Address()
            
            let addReservationViewController = segue.destinationViewController as! AddReservationViewController
            let reservationId = requestHelper.addReservation()
            reservation = requestHelper.getReservation(reservationId)
            person = requestHelper.getPerson(reservation.personId)
            address = requestHelper.getAddress(person.addressId)
            person.address = address
            room = requestHelper.getRoomById(reservation.roomId)
            services = requestHelper.getServiceListByReservationId(reservation.id)
            
            reservation.room = room
            reservation.person = person
            reservation.services = services
            addReservationViewController.reservation = reservation
        }
    }
    
}


