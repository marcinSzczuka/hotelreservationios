//
//  Room.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 21.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import Foundation
class Room: NSObject {
    var id: Int!
    var capacity: Int!
    var bed: Int!
    var radio: Bool!
    var tv: Bool!
    var bathroom: Bool!
    var phone: Bool!
    var wifi: Bool!
    var balcony: Bool!
    var seeView: Bool!
    
    override var description: String {
        return "Room[id=\(id), capacity=\(capacity), bed=\(bed), radio=\(radio), tv=\(tv), bathroom=\(bathroom), phone=\(phone), wifi=\(wifi), balcony=\(balcony), seeView=\(seeView)]"
    }
}