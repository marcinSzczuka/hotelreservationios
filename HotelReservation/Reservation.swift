//
//  Reservation.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 19.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import Foundation

class Reservation: NSObject {
    var id: Int = 0
    var personId: Int = 0
    var roomId: Int = 0
    var from: NSDate!
    var to: NSDate!
    var person: NSObject?
    var room: NSObject?
    var services: [Service] = []
    
    override var description: String {
        return "Reservation[id=\(id), personId=\(personId), roomId=\(roomId), from=\(from), to=\(to),  person=\(person), room=\(room), services=\(services)]"
    }
    
}
