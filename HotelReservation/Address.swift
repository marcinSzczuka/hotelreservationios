//
//  Address.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 20.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import Foundation

class Address: NSObject {
    var id: Int!
    var country: NSString!
    var city: NSString!
    var street: NSString!
    var number: NSString!
    var phone: NSString!
    var mail: NSString!
    
    override var description: String {
        return "Address[id=\(id), country=\(country), city=\(city), street=\(street), number=\(number), phone=\(phone), mail=\(mail)]"
    }
}