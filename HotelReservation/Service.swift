//
//  Service.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 21.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import Foundation
class Service: NSObject {
    var id: Int!
    var reservationId: Int!
    var from: NSDate!
    var to: NSDate!
    var quantity: Int!
    var type: NSString!
    
    override var description: String {
        return "Service[id=\(id), reservationId=\(reservationId), from=\(from), to=\(to), quantity=\(quantity), type=\(type)]"
    }
}
