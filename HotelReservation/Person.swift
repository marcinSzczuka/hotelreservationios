//
//  Person.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 20.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import Foundation
class Person: NSObject {
    var id: Int!
    var addressId: Int!
    var name: NSString!
    var lastname: NSString!
    var PESEL: NSString!
    var address: Address!
    
    override var description: String {
        return "Person[id=\(id), addressId=\(addressId), name=\(name), lastname=\(lastname), PESEL=\(PESEL), address=\(address)]"
    }
}