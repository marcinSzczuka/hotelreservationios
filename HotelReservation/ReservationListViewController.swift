//
//  ReservationListViewController.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 19.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import UIKit

class ReservationListViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var reservationListTable: UITableView!
    var jsonData: NSData!
    var objectParser: JsonParseObjectHelper = JsonParseObjectHelper()
    
    var reservationList = [Reservation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var jsonResults: AnyObject
        if(jsonData != nil) {
            do {
                jsonResults = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: [])
                reservationList = objectParser.parseReservationJson(jsonResults)
                print(reservationList)
            
            } catch {
                print("Fetch failed: \((error as NSError).localizedDescription)")
            }
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        reservationListTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reservationList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath)
        cell.textLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping;
        cell.textLabel?.numberOfLines = 0;
        cell.textLabel?.font = UIFont(name:"Avenir", size:9)
        cell.textLabel?.text = reservationList[indexPath.row].description
        return cell
    }
    
    
    
    }
