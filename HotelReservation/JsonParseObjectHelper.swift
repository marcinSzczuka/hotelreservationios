//
//  JsonParseObjectHelper.swift
//  HotelReservation
//
//  Created by Marcin Szczuka on 20.06.2016.
//  Copyright © 2016 Marcin Szczuka. All rights reserved.
//

import Foundation

class  JsonParseObjectHelper: NSObject {
    
    func parseReservationJson(anyObj:AnyObject) -> Array<Reservation>{
        
        var list:Array<Reservation> = []
        
        if  anyObj is Array<AnyObject> {
            
            for json in anyObj as! Array<AnyObject>{
                print(json)
                let reservation:Reservation = Reservation()
                reservation.id = json["id"]!!.integerValue
                reservation.personId = json["person"]!!.integerValue
                reservation.roomId = json["room"]!!.integerValue
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss.SSS"
                dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 1)
                let from: NSString = json["from"] as! NSString
                let fromString: String = String(from)
                reservation.from = dateFormatter.dateFromString(fromString)
                
                let to: NSString = json["to"] as! NSString
                let toString: String = String(to)
                reservation.to = dateFormatter.dateFromString(toString)
                list.append(reservation)
            }// for
            
        } // if
        
        return list
        
    }//func

    func parseServiceJson(anyObj:AnyObject) -> Array<Service>{
        
        var list:Array<Service> = []
        
        if  anyObj is Array<AnyObject> {
            
            for json in anyObj as! Array<AnyObject>{
                print(json)
                let service:Service = Service()
                
                service.id = json["id"]!!.integerValue
                service.reservationId = json["reservation"]!!.integerValue
                service.quantity = json["quantity"]!!.integerValue
                service.type = json["type"] as! NSString
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 1)
                dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss.SSS"
                
                let from: NSString = json["from"] as! NSString
                let fromString: String = String(from)
                service.from = dateFormatter.dateFromString(fromString)
                
                let to: NSString = json["to"] as! NSString
                let toString: String = String(to)
                service.to = dateFormatter.dateFromString(toString)
                list.append(service)
            }// for
            
        } // if
        
        return list
        
    }//func
    
    
    func parsePersonJson(anyObj:AnyObject) -> Array<Person>{
        
        var list:Array<Person> = []
        
        if  anyObj is Array<AnyObject> {
            
            for json in anyObj as! Array<AnyObject>{
                print(json)
                let person:Person = Person()
                person.id = json["id"]!!.integerValue
                person.addressId = json["address"]!!.integerValue
                person.name = json["name"] as! NSString
                person.lastname = json["lastname"] as! NSString
                person.PESEL = json["PESEL"] as! NSString
                list.append(person)
            }// for
            
        } // if
        
        return list
        
    }//func
    
    func parseAddressJson(anyObj:AnyObject) -> Array<Address>{
        
        var list:Array<Address> = []
        
        if  anyObj is Array<AnyObject> {
            
            for json in anyObj as! Array<AnyObject>{
                print(json)
                let address:Address = Address()
                address.id = json["id"]!!.integerValue
                address.country = json["country"] as! NSString
                address.city = json["city"] as! NSString
                address.street = json["street"] as! NSString
                address.number = json["number"] as! NSString
                address.phone = json["phone"] as! NSString
                address.mail = json["mail"] as! NSString
                list.append(address)
            }// for
            
        } // if
        
        return list
        
    }//func
    
    func parseRoomJson(anyObj:AnyObject) -> Array<Room>{
        
        var list:Array<Room> = []
        
        if  anyObj is Array<AnyObject> {
            
            for json in anyObj as! Array<AnyObject>{
                print(json)
                let room:Room = Room()
                room.id = json["id"]!!.integerValue
                room.bed = json["bed"]!!.integerValue
                room.capacity = json["capacity"]!!.integerValue
                room.balcony = (json["balcony"]!!.integerValue).toBool()
                room.bathroom = (json["bathroom"]!!.integerValue).toBool()
                room.phone = (json["phone"]!!.integerValue).toBool()
                room.radio = json["radio"]!!.integerValue.toBool()
                room.seeView = (json["seeView"]!!.integerValue).toBool()
                room.wifi = (json["WIFI"]!!.integerValue).toBool()
                room.tv = (json["tv"]!!.integerValue).toBool()
                list.append(room)
            }// for
            
        } // if
        
        return list
        
    }//func


}


extension Int {
    func toBool() -> Bool? {
        switch self {
        case 1:
            return true
        case 0:
            return false
        default:
            return nil
        }
    }
}

